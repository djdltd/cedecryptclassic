// application icons
#define IDI_ICON 			1
#define IDI_ICONSMALL		2

// Function Specific Identifiers
#define	FS_CENTER			100
#define FS_BOTTOMRIGHT		101
#define FS_STYLESTANDARD	102
#define FS_STYLEBLANK		103

// Custom control message identifiers
#define WM_UICOMMAND			4000
#define WM_UIHIGHLIGHT			4001
#define WM_UINOHIGHLIGHT		4002
#define WM_MESSAGINGEVENT		4005
#define WM_UISCROLL				4006

// Bitmap identifiers
#define IDB_STDBTNBASE		200
#define IDB_STDBTNHIGH		201
#define IDB_STDBTNDOWN		202
#define IDB_DIAGHEADER		243
#define IDB_VSCROLLBASE		296

//Custom Control identifiers
#define CID_TEST			400
#define CID_HEADER			413
#define CID_MULTISCROLL		414

// Windows control identifiers
#define ID_DIAGLIST			701
#define IDM_FILE_EXIT		703
#define IDM_HELP_ABOUT		704
#define IDM_TOOLS_OPTIONS	705
#define ID_WEBVERSION		706
#define ID_LATESTUPDATES	707
#define ID_NUMFILES			708
#define ID_LOCALFILE1		709
#define ID_WEBLOCATION1		710
#define ID_LOCALFILE2		711
#define ID_WEBLOCATION2		712
#define ID_LOCALFILE3		713
#define ID_WEBLOCATION3		714
#define ID_LOCALFILE4		715
#define ID_WEBLOCATION4		716
#define ID_LOCALFILE5		717
#define ID_WEBLOCATION5		718
#define ID_LOCALFILE6		719
#define ID_WEBLOCATION6		720
#define ID_LOCALFILE7		721
#define ID_WEBLOCATION7		722
#define ID_STATIC			723
#define ID_BTNSAVE			724
#define ID_BTNLOAD			725

// Custom application identifiers
// and messages
#define CRYPT_MSG				903
#define CRYPT_PROGRESS1			904
#define CRYPT_PROGRESS2			905
#define CRYPT_PROGRESSSINGLE	906
#define CRYPT_COMPLETE			907

// Internal buffer sizes
#define SIZE_NAME			64
#define SIZE_STRING			1024
#define SIZE_LARGESTRING	10000
#define SIZE_INTEGER		32

// Application timer messages

// Network control identifiers


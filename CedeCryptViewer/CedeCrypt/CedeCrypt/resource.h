//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AppMain.rc
//
#define IDI_ICON                        1
#define IDI_ICONSMALL                   2
#define SIZE_SMALLINT                   16
#define SIZE_INTEGER                    32
#define SIZE_NAME                       64
#define FS_CENTER                       100
#define FS_BOTTOMRIGHT                  101
#define IDD_DIALOG1                     101
#define FS_STYLESTANDARD                102
#define FS_STYLEBLANK                   103
#define IDB_TEXTPUBLICKEY				105
#define IDB_BINARY2                     107
#define IDB_BINARY3                     108
#define IDB_STDBTNBASE                  200
#define IDB_STDBTNHIGH                  201
#define IDB_STDBTNDOWN                  202
#define IDB_DIAGHEADER                  243
#define IDB_VSCROLLBASE                 296
#define IDB_HEADER                      297
#define IDB_PASSHEADER                  298
#define IDB_ABOUTHEADER                 299
#define IDB_LICENSEIMAGE                300
#define IDB_PUBLICKEY                   300
#define CID_TEST                        400
#define CID_HEADER                      413
#define CID_MULTISCROLL                 414
#define CID_PASSWORDOK                  415
#define CID_PASSWORDCANCEL              416
#define CID_CHECKSHELL                  417
#define CID_LICENSEWINDOWOK             418
#define CID_LICENSEWINDOWCANCEL         419
#define CID_STATIC                      420
#define CID_CHECKCOMMAND                421
#define CID_SESSIONDECRYPT              422
#define ID_DIAGLIST                     701
#define IDM_FILE_EXIT                   703
#define IDM_HELP_ABOUT                  704
#define IDM_TOOLS_OPTIONS               705
#define IDM_FILE_TEST                   706
#define IDM_FILE_RECOVER                707
#define ID_GROUPBOX                     800
#define ID_OPTFILE                      801
#define ID_OPTFOLDER                    802
#define ID_GROUPBOX2                    803
#define ID_GROUPBOX3                    804
#define ID_EDITSOURCE                   805
#define ID_EDITDEST                     806
#define ID_BTNBEGIN                     807
#define ID_BTNSRCBROWSE                 809
#define ID_BTNDSTBROWSE                 810
#define ID_BTNEXIT                      811
#define ID_LSTSTATUS                    812
#define ID_EDITPASSWORD                 813
#define ID_BTNPASSOK                    814
#define ID_BTNPASSCANCEL                815
#define ID_PROGRESS                     816
#define ID_GROUPBOX1                    817
#define ID_OPTENCRYPT                   818
#define ID_OPTDECRYPT                   819
#define ID_BTNABOUTOK                   820
#define ID_LBLPHASE                     821
#define ID_CHKDELETEORIG                822
#define ID_BTNCANCEL                    823
#define ID_TOTALPROGRESS                824
#define ID_LBLTOTAL                     825
#define IDC_TXTLICENSE                  826
#define IDC_TXTLICOK                    827
#define IDC_TXTLICCANCEL                828
#define IDM_ENTERLICENSE                829
#define IDM_FILE_PDIR                   830
#define ID_LSTFOLDERS                   831
#define ID_BTNADDFOLDER                 832
#define ID_BTNREMFOLDER                 833
#define ID_LBLSTATIC                    834
#define IDD_WAITDLG                     835
#define ID_LBLSESSIONINFO				836
#define CRYPT_MSG                       903
#define CRYPT_PROGRESS1                 904
#define CRYPT_PROGRESS2                 905
#define CRYPT_PROGRESSSINGLE            906
#define CRYPT_COMPLETE                  907
#define CRYPT_USERPROGRESS              908
#define CRYPT_RPROGRESSSINGLE           909
#define CRYPT_RUSERPROGRESS             910
#define ID_BTNTEXTCLOSE					911
#define ID_TXTDECRYPTED					912
#define IDM_TOOLS_UTE					913
#define ID_UTEBTNOK						914
#define ID_UTEBTNCANCEL					915
#define ID_UTEBTNENABLE					916
#define ID_UTELBLHOTENC					917
#define ID_UTELBLHOTDEC					918
#define ID_UTEBTNHOTENCCTRL				919
#define ID_UTEBTNHOTDECCTRL				920
#define ID_UTEBTNHOTENCALT				921
#define ID_UTEBTNHOTDECALT				922
#define ID_UTETXTHOTENC					923
#define ID_UTETXTHOTDEC					924
#define ID_UTEBTNHOTENCLISTEN			925
#define ID_UTEBTNHOTDECLISTEN			926
#define ID_UTELBLHOTKEYSTATUS			927
#define ID_LSTOPTIONCATEGORY			929
#define ID_CHKPFOLDERPROMPTPUP				930
#define ID_OPTWINDOWCLOSE				931
#define ID_PFOLDERREQUESTOK				932
#define ID_PFOLDERREQUESTGROUP		933
#define ID_OPTDOACTION						934
#define ID_OPTDONOTHING					935
#define CID_PFOLDERREQUESTOK			936
#define ID_CHKPFOLDERPROMPTPDN		937
#define ID_CHKAUTOUPDATECHECK		938
#define IDM_CHECKUPDATES					939
#define ID_EDITPASSWORD2					940

#define ALG_CYBERCEDE					100
#define ALG_AES256						101
#define ALG_AES512						102
#define ALG_DES							103
#define ALG_3DES						104



#define IDT_UPDATECAPTION               1005
#define IDT_ACTIONCHECK                 1006
#define IDT_DLGWAITCHECK				1007
#define SIZE_STRING                     1024
#define WM_UICOMMAND                    4000
#define WM_UIHIGHLIGHT                  4001
#define WM_UINOHIGHLIGHT                4002
#define WM_MESSAGINGEVENT               4005
#define WM_UISCROLL                     4006
#define SIZE_LARGESTRING                10000
#define SIZE_VERYLARGESTRING            32000
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
